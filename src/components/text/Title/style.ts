import styled from 'styled-components'

export const Title = styled.h1<{ color: string }>`
  font-size: 36px;
  margin: 0;
  font-weight: bold;
  color: ${props => props.color};
`
