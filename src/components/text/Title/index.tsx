import React from 'react'
import * as S from './style'

type Props = {
  text: string
  color?: string
}

const Title = ({ text, color = '#000' }: Props) => {
  return <S.Title color={color}>{text}</S.Title>
}

export default Title
