import styled from 'styled-components'

export const Label = styled.label<{ error: boolean }>`
  font-size: 16px;
  margin: 0;
  color: ${props => (props.error ? 'red' : 'black')};
`
