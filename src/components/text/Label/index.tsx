import React from 'react'
import * as S from './style'

type Props = {
  text: string
  error?: boolean
  required?: boolean
}

const Label = ({ text, error = false, required = false }: Props) => {
  return (
    <S.Label error={error}>
      {text} {required && <span style={{ color: 'orangered' }}>*</span>}
    </S.Label>
  )
}

export default Label
