import styled from 'styled-components'

export const Button = styled.div<{ type: 'warning' | 'success'; disabled: boolean }>`
  padding: 0.3rem 0;
  font-size: 14px;
  border-radius: 04px;
  color: ${props => (props.type === 'warning' ? '#FFF' : '#000')};
  background: ${props => (props.type === 'warning' ? 'red' : '#CCCCCC')};
  cursor: ${props => (props.disabled ? 'not-allowed' : 'pointer')};
  height: 30px;
  display: flex;
  align-items: center;
  justify-content: center;
`
