import React from 'react'
import * as S from './style'

type Props = {
  value: string
  onClick: () => void
  type?: 'success' | 'warning'
  disabled?: boolean
}

const Button = ({ value, onClick, type = 'success', disabled = false }: Props) => {
  return (
    <S.Button
      type={type}
      onClick={() => {
        if (!disabled) {
          onClick()
        }
      }}
      disabled={disabled}
    >
      {value}
    </S.Button>
  )
}

export default Button
