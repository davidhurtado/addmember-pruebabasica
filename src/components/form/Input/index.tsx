import React from 'react'
import Label from '../../text/Label'
import * as S from './style'

type Props = {
  label: string
  placeholder?: string
  onChange: (e: React.FormEvent<HTMLInputElement>) => void
  required?: boolean
  type?: string
}

const Input = ({ label, placeholder, onChange, required, type = 'text' }: Props) => {
  return (
    <S.Container>
      <div>
        <Label text={label} required={required} />
      </div>
      <S.Input placeholder={placeholder} onChange={onChange} type={type} />
    </S.Container>
  )
}

export default Input
