import styled from 'styled-components'

export const Input = styled.input`
  border-radius: 4px;
  margin-top: 0.5rem;
  height: 30px;
  width: 98.5%;
  border: 1px solid #ccc;
`

export const Container = styled.div`
  display: block;
  text-align: left;
`
