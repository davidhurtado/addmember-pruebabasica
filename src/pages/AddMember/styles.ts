import styled from 'styled-components'

export const Container = styled.div`
  width: 100vw;
  height: 100vh;
  background-color: #ccc;
  padding: 1rem;
`

export const ContainerGrid = styled.div`
  display: flex;
  width: 100%;
`

export const LeftContainer = styled.div`
  width: 20%;
`

export const FormContainer = styled.div`
  width: 74%;
  padding: 1rem 1rem 2rem;
  border-radius: 1rem;
  background: #fff;
  display: flex;
  gap: 1rem;
`
export const HalfForm = styled.div`
  width: 49%;
`

export const Row = styled.div`
  display: flex;
  justify-content: space-between;
  gap: 1rem;
`

export const FormSection = styled.div`
  margin-bottom: 1rem;
`

export const Subtitle = styled.h4`
  font-size: 16px;
  margin: 0 0 0.5rem;
  color: orangered;
  font-weight: 600;
`
