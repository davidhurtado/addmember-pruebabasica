import axios from 'axios'
import { useFormik } from 'formik'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import * as Yup from 'yup'
import Button from '../../components/form/Button'
import Input from '../../components/form/Input'
import Label from '../../components/text/Label'
import Title from '../../components/text/Title'
import * as S from './styles'

type Member = {
  first_name: string
  last_name: string
  middle_name: string
  employee_number: string
  home_phone: string
  cell_phone: string
  web_active: string
  suffix: string
  retire_date: string
  rdray: string
  department: string
  unit: string
  title_id: string
  gender_id: string
  ethnicity_id: string
  union_membership_status_id: number
  employ_date: string
  social_security_number: string
  email: string
  birth_date: string
  iaff_member_number: string
  join_date: string
  sick_plan_id: number
  address_line_1: string
  address_line_2: string
  city: string
  zip_code: string
  state: string
  position_id: string
  status_id: string
  station_id: string
  base_hour_id: string
  member_id: string
  badge_number: string
  promo_date: string
  salary_review_date: string
  benefit_date: string
  termination_date: string
  batallion_number: string
  shift: string
  budget_position: string
  grade: string
  class: string
  hourly_rate: string
}

const initialData = {
  first_name: '',
  last_name: '',
  middle_name: '',
  employee_number: '',
  home_phone: '',
  cell_phone: '',
  web_active: '',
  suffix: '',
  retire_date: '',
  rdray: '',
  department: '',
  unit: '',
  title_id: '',
  gender_id: '',
  ethnicity_id: '',
  union_membership_status_id: 0,
  employ_date: '',
  social_security_number: '',
  email: '',
  birth_date: '',
  iaff_member_number: '',
  join_date: '',
  sick_plan_id: 0,
  address_line_1: '',
  address_line_2: '',
  city: '',
  zip_code: '',
  state: '',
  position_id: '',
  status_id: '',
  station_id: '',
  base_hour_id: '',
  member_id: '',
  badge_number: '',
  promo_date: '',
  salary_review_date: '',
  benefit_date: '',
  termination_date: '',
  batallion_number: '',
  shift: '',
  budget_position: '',
  grade: '',
  class: '',
  hourly_rate: '',
}

const validationSchema = Yup.object().shape({
  first_name: Yup.string().required('Required'),
  last_name: Yup.string().required('Required'),
  social_security_number: Yup.string().required('Required'),
  email: Yup.string().required('Required'),
  birth_date: Yup.string().required('Required'),
  join_date: Yup.string().required('Required'),
})

const AddMember = () => {
  const [userToken, setUserToken] = useState('')
  const [loading, setLoading] = useState(false)
  const [member, setMember] = useState<Member>(initialData)
  const navigate = useNavigate()

  const formik = useFormik({
    initialValues: initialData,
    enableReinitialize: true,
    validateOnMount: true,
    validateOnBlur: true,
    validateOnChange: true,
    validationSchema,
    onSubmit: values => {
      setLoading(true)
      axios
        .post(`https://uniontracking-3.frogi.dev/api/member`, values)
        .then(res => {
          if (res?.data?.data?.token) {
            localStorage.setItem('token', res.data.data.token)
            toast.info('Member created')
          }
        })
        .catch(err => {
          toast.error('Error creating member, try again')
        })
        .finally(() => {
          setLoading(false)
        })
    },
  })

  useEffect(() => {
    const token = localStorage.getItem('token')
    if (token) {
      setUserToken(token)
    } else {
      navigate('/login')
    }
  }, [])

  const validateData = () => {
    if (Object.keys(formik.errors).length > 0) {
      console.error(formik.errors)
      toast.error('Please fill the required fields')
      return
    }
    formik.handleSubmit()
  }

  const onChanteInput = (e: React.FormEvent<HTMLInputElement>, field: string) => {
    formik.setFieldValue(field, e.currentTarget.value)
  }

  return (
    <S.Container>
      <Title text="Add Member" color="blue" />
      <S.ContainerGrid>
        <S.LeftContainer></S.LeftContainer>
        <S.FormContainer>
          <S.HalfForm>
            <S.Subtitle>Personal Info</S.Subtitle>
            <S.Row>
              <S.HalfForm>
                <S.FormSection>
                  <Input label="Title" onChange={e => onChanteInput(e, 'title_id')} />
                </S.FormSection>
              </S.HalfForm>
              <S.HalfForm>
                <S.FormSection>
                  <Input label="Last Name" onChange={e => onChanteInput(e, 'last_name')} required />
                </S.FormSection>
              </S.HalfForm>
            </S.Row>
            <S.Row>
              <S.HalfForm>
                <S.FormSection>
                  <Input label="First Name" required onChange={e => onChanteInput(e, 'first_name')} />
                </S.FormSection>
              </S.HalfForm>
              <S.HalfForm>
                <S.FormSection>
                  <Input label="Middle Name" onChange={e => onChanteInput(e, 'middle_name')} />
                </S.FormSection>
              </S.HalfForm>
            </S.Row>
            <S.FormSection>
              <Input label="Suffix (Type in custom suffix)" onChange={e => onChanteInput(e, 'suffix')} />
            </S.FormSection>
            <S.FormSection>
              <Input label="Address (1)" onChange={e => onChanteInput(e, 'address_line_1')} />
            </S.FormSection>
            <S.FormSection>
              <Input label="Address (2)" onChange={e => onChanteInput(e, 'address_line_2')} />
            </S.FormSection>
            <S.Row>
              <S.HalfForm>
                <S.FormSection>
                  <Input label="Zip Code" onChange={e => onChanteInput(e, 'zip_code')} />
                </S.FormSection>
              </S.HalfForm>
              <S.HalfForm>
                <S.FormSection>
                  <Input label="State" onChange={e => onChanteInput(e, 'state')} />
                </S.FormSection>
              </S.HalfForm>
            </S.Row>
            <S.Row>
              <S.HalfForm>
                <S.FormSection>
                  <Input label="City" onChange={e => onChanteInput(e, 'city')} />
                </S.FormSection>
              </S.HalfForm>
              <S.HalfForm>
                <S.FormSection>
                  <Input label="Social Security #" required onChange={e => onChanteInput(e, 'social_security_number')} />
                </S.FormSection>
              </S.HalfForm>
            </S.Row>
            <S.Row>
              <S.HalfForm>
                <S.FormSection>
                  <Input label="Employee Number" onChange={e => onChanteInput(e, 'employee_number')} />
                </S.FormSection>
              </S.HalfForm>
              <S.HalfForm>
                <S.FormSection>
                  <Input label="IAFF#" onChange={e => onChanteInput(e, 'iaff_member_number')} />
                </S.FormSection>
              </S.HalfForm>
            </S.Row>
          </S.HalfForm>
          <S.HalfForm>
            <S.Subtitle>Member Status</S.Subtitle>
            <S.Row>
              <S.HalfForm>
                <S.FormSection>
                  <input type="radio" checked={formik.values.status_id === '13'} onClick={() => formik.setFieldValue('status_id', '13')} />

                  <Label text="Initiated" required></Label>
                </S.FormSection>
              </S.HalfForm>
              <S.HalfForm>
                <S.FormSection>
                  <input type="radio" checked={formik.values.status_id === '12'} onClick={() => formik.setFieldValue('status_id', '12')} />
                  <Label text="Transferred In" required></Label>
                </S.FormSection>
              </S.HalfForm>
            </S.Row>
            <S.Subtitle>Demographics</S.Subtitle>
            <S.Row>
              <S.HalfForm>
                <S.FormSection>
                  <Input label="Date of birth" required onChange={e => onChanteInput(e, 'birth_date')} type="date" />
                </S.FormSection>
              </S.HalfForm>
              <S.HalfForm>
                <S.FormSection>
                  <Input label="Join Date" required onChange={e => onChanteInput(e, 'join_date')} type="date" />
                </S.FormSection>
              </S.HalfForm>
            </S.Row>
            <S.Row>
              <S.HalfForm>
                <S.FormSection>
                  <Input label="Gender" onChange={e => onChanteInput(e, 'gender_id')} />
                </S.FormSection>
              </S.HalfForm>
              <S.HalfForm>
                <S.FormSection>
                  <Input label="Race" onChange={e => onChanteInput(e, 'shift')} />
                </S.FormSection>
              </S.HalfForm>
            </S.Row>
            <S.Subtitle>Job Info</S.Subtitle>
            <S.Row>
              <S.HalfForm>
                <S.FormSection>
                  <Input label="Original Hire" required onChange={e => onChanteInput(e, 'shift')} />
                </S.FormSection>
              </S.HalfForm>
              <S.HalfForm>
                <S.FormSection>
                  <Input label="Badge #" onChange={e => onChanteInput(e, 'badge_number')} />
                </S.FormSection>
              </S.HalfForm>
            </S.Row>
            <S.FormSection>
              <Input label="Position" onChange={e => onChanteInput(e, 'position_id')} />
            </S.FormSection>
            <S.FormSection>
              <Input label="Sick Plan" required onChange={e => onChanteInput(e, 'sick_plan_id')} />
            </S.FormSection>
            <S.Subtitle>Contact Detail</S.Subtitle>
            <S.Row>
              <S.HalfForm>
                <S.FormSection>
                  <Input label="Home Phone" onChange={e => onChanteInput(e, 'home_phone')} />
                </S.FormSection>
              </S.HalfForm>
              <S.HalfForm>
                <S.FormSection>
                  <Input label="Cell Phone" onChange={e => onChanteInput(e, 'cell_phone')} />
                </S.FormSection>
              </S.HalfForm>
            </S.Row>
            <S.FormSection>
              <Input label="Email Address" required onChange={e => onChanteInput(e, 'email')} />
            </S.FormSection>
            <br />
            <br />
            <S.Row>
              <S.HalfForm>
                <S.FormSection>
                  <Button value={loading ? 'Loading...' : 'Create'} onClick={validateData} type="warning" disabled={loading} />
                </S.FormSection>
              </S.HalfForm>
              <S.HalfForm>
                <S.FormSection>
                  <Button value="Close" onClick={() => setMember(initialData)} type="success" />
                </S.FormSection>
              </S.HalfForm>
            </S.Row>
          </S.HalfForm>
        </S.FormContainer>
      </S.ContainerGrid>
    </S.Container>
  )
}

export default AddMember
