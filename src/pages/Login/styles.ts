import styled from 'styled-components'
import loginBG from '../../assets/loginBG.jpeg'

export const ContainerLogin = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  background-image: url(${loginBG});
  background-repeat: no-repeat;
  background-size: cover;
`

export const DialogModal = styled.div`
  width: 25%;
  padding: 2rem 3rem;
  border-radius: 1rem;
  background: #fff;
`

export const FormSection = styled.div`
  margin-bottom: 1rem;
`
