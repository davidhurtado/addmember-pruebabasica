import axios from 'axios'
import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import Button from '../../components/form/Button'
import Input from '../../components/form/Input'
import * as S from './styles'

const Login = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const navigate = useNavigate()

  const login = () => {
    axios
      .post(`https://uniontracking-3.frogi.dev/api/login`, { email, password })
      .then(res => {
        console.log('RSPUESTA', res.data.data)
        if (res?.data?.data?.token) {
          localStorage.setItem('token', res.data.data.token)
          navigate('/add-member')
        }
      })
      .catch(err => {
        toast.error('Wrong credentials')
      })
  }

  return (
    <S.ContainerLogin>
      <S.DialogModal>
        <S.FormSection>
          <Input label="Email" onChange={e => setEmail(e.currentTarget.value)} />
        </S.FormSection>
        <S.FormSection>
          <Input label="Password" onChange={e => setPassword(e.currentTarget.value)} />
        </S.FormSection>
        <br />
        <Button value="Log in" onClick={login} type="success" />
      </S.DialogModal>
    </S.ContainerLogin>
  )
}

export default Login
